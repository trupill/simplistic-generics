{-# LANGUAGE GADTs            #-}
{-# LANGUAGE TypeFamilies     #-}
{-# LANGUAGE ConstraintKinds  #-}
{-# LANGUAGE TypeOperators    #-}
{-# LANGUAGE FlexibleContexts #-}
module Generics.Simplistic.Derive.Eq where

import Data.Proxy

import Generics.Simplistic
import Generics.Simplistic.Util

geq' :: (OnLeaves Eq f) => SRep I f -> SRep I f -> Bool
geq' x y = maybe False sameLeaves $ zipSRep x y
  where
    sameLeaves :: (OnLeaves Eq f) => SRep (I :*: I) f -> Bool
    sameLeaves = repLeavesC (Proxy :: Proxy Eq) (uncurry' (==)) (&&) True

geq :: (Generic a, GShallow (Rep a), OnLeaves Eq (Rep a))
    => a -> a -> Bool
geq x y = geq' (fromS x) (fromS y)
